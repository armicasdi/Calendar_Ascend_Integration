<?php

    include_once 'apiConnection.php';
    include_once '../include/database.php';
    include_once 'databaseFunctions.php';
    include_once 'practiceProcedure.php';
    

    
    
    function getColorCategories () {

        global $OrganizationID;
        global $token;
        global $baseUrl;

        $curl = curl_init($baseUrl.'colorcategories');

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer '. $token,
            'Organization-ID: '. $OrganizationID,
        ));

        $output = curl_exec($curl);

        curl_close($curl);

        return $output;
    }

    function saveOnDatabase() {

        $data = json_decode(getColorCategories(), true);

        if ($data['statusCode'] =='200') {
            foreach ($data['data'] as $colorcategorie) {

                foreach ($colorcategorie['practiceProcedures'] as $practiceProcedure) {
                    $practiceProcedureData = json_decode(getPracticeProcedure($practiceProcedure['id']), true);
                    if ($practiceProcedureData['statusCode'] == '200') {
                        $practiceProcedureData = $practiceProcedureData['data'];

                        $procedureData = [
                            'name' => $practiceProcedureData['description'],
                            'ada_code' => $practiceProcedureData['adaCode'],
                            'api_id' => $practiceProcedure['id'],
                            'categories_name' => $colorcategorie['name'],
                            'categories_color' => $colorcategorie['color'],
                            'categories_id' => $colorcategorie['id'],
                            'categories_location' => $colorcategorie['location']['id'],    
                        ];
                        
                        if (findInDatabase('practice_procedures', $practiceProcedure['id'], 'api_id')) {
                            echo "Inserted On Database";
                            $result = updateInDatabase('practice_procedures', $procedureData, $practiceProcedure['id'], 'api_id');
                            echo $result;
                         } else {
                            echo "updated on Database";
                            $result = insertInDatabase('practice_procedures', $procedureData);
                        }
                    }
                    
                }
            }
        }
    }

    if (isset($_GET['action']) ) {
        switch ($_GET['action']) {
            case 'syncPracticeProcedures':
                saveOnDatabase();
            break;
        }
    }


