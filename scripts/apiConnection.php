<?php

    $token = [];
    $baseUrl = "https://prod.hs1api.com/ascend-gateway/api/v1/";
    $baseUrlv0 = "https://prod.hs1api.com/ascend-gateway/api/v0/";
    $client_id = "QURdW4NMP8nFunWXWb3OlGyE8LWcUrnS";
    $client_secret = "MK3ZrHePsbvGM5ww";
    $OrganizationID = "61b7cfbb7caef72b181bb564";

    $locations = [
        "manassas" => "14000000000040",
        "fairfax" => "14000000000041",
        "woodbridge" => "14000000000042"
    ];

    $idClinica = [
        "14000000000040" => '2', // Manassas
        "14000000000041" => '1', //Fairfax
        "14000000000042" => '3' //Woodbridge
    ];
    $providers = [
        "14000000000040" => '2', //Manassas
        "14000000000041" => '1', //Fairfax
        "14000000000042" => "3" // Woodbridge
    ];
    
    function getToken($client_id, $client_secret)
    {
        $curl = curl_init('https://prod.hs1api.com/oauth/client_credential/accesstoken?grant_type=client_credentials');
        curl_setopt($curl, CURLOPT_POSTFIELDS, "client_id=" . $client_id . "&client_secret=" . $client_secret );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded'
        ));

        $output = curl_exec($curl);
        curl_close($curl);

        if (!empty(curl_error($curl))) {
            $error = curl_error($curl);
            print_r($error);
        }

        $result = json_decode($output, true);

        return $result;

    }

    $resultAuthorization = getToken($client_id, $client_secret);

    if (!empty($resultAuthorization['access_token'])) {
        $token = $resultAuthorization['access_token'];
    }
