<?php

    require '../include/database.php';

    function findInDatabase($table, $id, $column, $return = false) {

        global $conexion;

        $sql = "SELECT * from $table WHERE $column = '$id'";
        $result = $conexion->query($sql);

        if (!$result) {
            $error = $conexion->error;
            echo $error;
            return false;
        }

        if ($result->num_rows == 0) {
            return false;
        } else {
            if ($return) {
                return $result->fetch_all(MYSQLI_ASSOC);
            }
            return true;
        }
    }

    function insertInDatabase($table, $data) {
        global $conexion;

        $c = implode(",", array_keys($data));
        $d = implode(", ", array_values($data));

        $template ="";
        $values = "";
        $result = [];


        foreach ($data as $columns) {
            if (!empty($template)) {
                $template .= ",?";            
            } else {
                $template .="?";
            }
            $values .="s";
        }

        $sql = "INSERT INTO $table ($c) VALUES ($template)";
        $conn = $conexion->prepare($sql);
        if (!$conn) {
            $error = $conexion->error;
            echo $error;
            return false;
        }
        $conn->bind_param($values, ...array_values($data));
        $res = $conn->execute();
        $result = $conn->get_result();
       

        if ($res) {
            return true;
        } else {
            $error = $conexion->error;
            $error2 = $conn->error;
        
           return false;
        }
    }

    function updateInDatabase($table, $data, $id, $column = null): bool {
        echo "In update Function";
        
        $values ="";
        global $conexion;

        foreach ($data as $key => $value) {
            $values .= empty($values)? "" : ",";
            $values .= "$key = '$value'" ;
        }

        //$columnId = !empty($column) ?? $id;

        $sql = "UPDATE $table SET $values WHERE $column = '$id' ";
        $result = $conexion->query($sql);

        if ($result) {
            return true;
        } else {
            $error = $conexion->error;

            return false;
        }


    }