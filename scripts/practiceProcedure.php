<?php

    include_once 'apiConnection.php';
    include_once '../include/database.php';
    

    $endpointpp = 'practiceprocedures/';
    $requestUrlpp = $baseUrl . $endpointpp;
    
    function getPracticeProcedures () {

        global $OrganizationID;
        global $token;
        global $requestUrlpp;

        $curl = curl_init($requestUrlpp);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer '. $token,
            'Organization-ID: '. $OrganizationID,
        ));

        $output = curl_exec($curl);

        curl_close($curl);

        return $output;
    }

    function getPracticeProcedure ($id) {

        global $OrganizationID;
        global $token;
        global $requestUrlpp;

        $curl = curl_init($requestUrlpp.$id);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer '. $token,
            'Organization-ID: '. $OrganizationID,
        ));

        $output = curl_exec($curl);

        curl_close($curl);

        return $output;
    }

    

    /* if (isset($_GET['action']) ) {
        switch ($_GET['action']) {
            case 'syncPracticeProcedures':
                saveOnDatabase();
            break;
        }
    } */


