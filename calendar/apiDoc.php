<?php

    /////////////////Constants //////////////////
    
    $token = "";
    $client_id = "QURdW4NMP8nFunWXWb3OlGyE8LWcUrnS"; //Topdental Liver App
    $client_secret = "MK3ZrHePsbvGM5ww";    //Topdental Liver App
    $OrganizationID = "61b7cfbb7caef72b181bb564"; //Topdental Liver App


    ////////////////////Token Retrieve /////////////////


    $curl = curl_init('https://prod.hs1api.com/oauth/client_credential/accesstoken?grant_type=client_credentials');
    curl_setopt($curl, CURLOPT_POSTFIELDS, "client_id=" . $client_id . "&client_secret=" . $client_secret );
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/x-www-form-urlencoded'
    ));

    $output = curl_exec($curl);
    curl_close($curl);

    if (!empty(curl_error($curl))) {
        $error = curl_error($curl);
        print_r($error);
    }

    $result = json_decode($output);

    $token = $result->access_token;


    ////////////////////Documents Logic//////////////////

        $data = [
            "name" => $_POST['name'],
           // "mimeType" => $_POST['mimeType'], // If need something different than PDF
            "mimeType" => "PDF",
            "ownerPatient" => [
                "id" => $_POST['owner_id'],
            ],
            "file" => [
                "content" => $_POST['content'], // Base64Pdf
            ],
        ];

        $url = "https://prod.hs1api.com/ascend-gateway/api/v0/documents";

        $curl = curl_init();

        $body = json_encode($data);
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>$body,
            CURLOPT_HTTPHEADER => array(
                'Organization-ID: ' . $OrganizationID,
                'Authorization: Bearer ' . $token,
                'Content-Type: application/json'
            ),
        ));

        $output = curl_exec($curl); // Code 201 if created

        curl_close($curl);

        $output = json_decode($output, true);

        $responseCode = $output['statusCode'];

        if ($responseCode == 200 || $responseCode == 201) {
            echo "Ok";
        } else {
            echo "Error";
        }

    ?>